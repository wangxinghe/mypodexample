//
//  MyObject.m
//  MyPodExample
//
//  Created by Ricol Wang on 15/04/2014.
//  Copyright (c) 2014 ricol wang. All rights reserved.
//

#import "MyObject.h"

@implementation MyObject

- (void)ShowMessage:(NSString *)message
{
    NSLog(@"Message: %@", message);
}

@end
