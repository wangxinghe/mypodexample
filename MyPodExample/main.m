//
//  main.m
//  MyPodExample
//
//  Created by Ricol Wang on 15/04/2014.
//  Copyright (c) 2014 ricol wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
