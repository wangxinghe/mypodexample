//
//  MyObject.h
//  MyPodExample
//
//  Created by Ricol Wang on 15/04/2014.
//  Copyright (c) 2014 ricol wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyObject : NSObject

- (void)ShowMessage:(NSString *)message;

@end
