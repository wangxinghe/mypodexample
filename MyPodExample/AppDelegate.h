//
//  AppDelegate.h
//  MyPodExample
//
//  Created by Ricol Wang on 15/04/2014.
//  Copyright (c) 2014 ricol wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
