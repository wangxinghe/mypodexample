Pod::Spec.new do |s|
  s.name         = "MyPodExample"
  s.version      = "0.0.1"
  s.summary      = "This is an example to show how to create a cocoapod"
  s.description  = <<-DESC
                   A longer description of MyPodExample in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC
  s.homepage     = "https://www.bitbucket.org"
  s.license      = { :type => "RMIT", :text => "this is a test!" }
  s.author             = { "Ricol Wang" => "wangxinghe1983@gmail.com" }
  s.source       = { :git => "https://wangxinghe@bitbucket.org/wangxinghe/mypodexample.git", :tag => "0.0.1" }
  s.source_files  = "MyPodExample/MyObject.{h,m}"
  s.requires_arc = true
end
